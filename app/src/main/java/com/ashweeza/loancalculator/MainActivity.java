//NAME:ASHWEEZA VAVILALA
//A03873224
//MOBILE PROGRAMMING HOME WORK1
//LOAN CALCULATOR

package com.ashweeza.loancalculator;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextView fivetv;
    TextView tentv;
    TextView fifteentv;
    TextView twentytv;
    TextView twentyfivetv;
    TextView thirtytv;
    double result5, result10, result15, result20, result25, result30;
    double interestrate;
    double principleamt;
    double x5;
    double x10;
    double x15;
    double x20;
    double x25;
    double x30;
    private EditText principleET;
    private EditText interestET;
    private TextWatcher principleTextWatcher = new TextWatcher() {
        //THE INPUT ELEMENT IS ATTACHED TO AN EDITABLE,
        //THEREFORE THESE METHODS ARE CALLED WHEN THE TEXT IS CHANGED

        public void onTextChanged(CharSequence s,
                                  int start, int before, int count) {
            //CATCH AN EXCEPTION WHEN THE INPUT IS NOT A NUMBER
            try {
                setprinciple((int) Double.parseDouble(s.toString()));

            } catch (NumberFormatException e) {
                setprinciple(0);
            }

            TextView monthly = (TextView) findViewById(R.id.textView3);
            monthly.setVisibility(View.VISIBLE);
            LinearLayout res = (LinearLayout) findViewById(R.id.result);
            res.setVisibility(View.VISIBLE);
        }

        public void afterTextChanged(Editable s) {
        }

        public void beforeTextChanged(CharSequence s,
                                      int start, int count, int after) {
        }
    };
    private TextWatcher interestTextWatcher = new TextWatcher() {
        //THE INPUT ELEMENT IS ATTACHED TO AN EDITABLE,
        //THEREFORE THESE METHODS ARE CALLED WHEN THE TEXT IS CHANGED

        public void onTextChanged(CharSequence s,
                                  int start, int before, int count) {
            //CATCH AN EXCEPTION WHEN THE INPUT IS NOT A NUMBER
            try {

                setinterest((int) Double.parseDouble(s.toString()));
            } catch (NumberFormatException e) {
                setinterest(0);
            }
            TextView monthly = (TextView) findViewById(R.id.textView3);
            monthly.setVisibility(View.VISIBLE);
            LinearLayout res = (LinearLayout) findViewById(R.id.result);
            res.setVisibility(View.VISIBLE);
        }

        public void afterTextChanged(Editable s) {

        }

        public void beforeTextChanged(CharSequence s,
                                      int start, int count, int after) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //THE INPUT FIELDS
        principleET = (EditText) findViewById(R.id.principle);
        interestET = (EditText) findViewById(R.id.interest);

        //OUTPUT FIELDS
        fivetv = (TextView) findViewById(R.id.textView5);
        tentv = (TextView) findViewById(R.id.textView10);
        fifteentv = (TextView) findViewById(R.id.textView15);
        twentytv = (TextView) findViewById(R.id.textView20);
        twentyfivetv = (TextView) findViewById(R.id.textView25);
        thirtytv = (TextView) findViewById(R.id.textView30);

        //PRINTING A WELCOME TOAST
        Toast.makeText(getApplicationContext(),
                "WELCOME TO LOAN CALCULATOR!!!!!!", Toast.LENGTH_LONG).show();
        loancalc();
        //CALLS A FUCNTION WHEN THERE IS A TEXT CHANGE
        principleET.addTextChangedListener(principleTextWatcher);
        interestET.addTextChangedListener(interestTextWatcher);

    }

    //THIS FUNCTION CALCULATES THE MONTHLY INSTALLMENTS
    private void loancalc() {

        double rate = (interestrate / 1200);
        x5 = Math.pow((1 + rate), 60);
        x10 = Math.pow((1 + rate), 120);
        x15 = Math.pow((1 + rate), 180);
        x20 = Math.pow((1 + rate), 240);
        x25 = Math.pow((1 + rate), 300);
        x30 = Math.pow((1 + rate), 360);

        result5 = (principleamt * rate * x5) / (x5 - 1);
        result10 = (principleamt * rate * x10) / (x10 - 1);
        result15 = (principleamt * rate * x15) / (x15 - 1);
        result20 = (principleamt * rate * x20) / (x20 - 1);
        result25 = (principleamt * rate * x25) / (x25 - 1);
        result30 = (principleamt * rate * x30) / (x30 - 1);
        displayInstallments();
    }

    //THIS FUNCTION POPS UP A DIALOG WHEN WE CLICK ON BACK BUTTON
    public void onBackPressed() {
        AlertDialog.Builder ad1 = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        ad1.setView(inflater.inflate(R.layout.dialogdesign, null));
        ad1.setMessage("Do you want to exit?");
        ad1.setCancelable(false);
        ad1.setIcon(R.mipmap.alert);
        ad1.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getApplicationContext(),
                        "Clicked NO!", Toast.LENGTH_SHORT).show();
            }
        });
        ad1.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.finish();
            }
        });
        AlertDialog alert = ad1.create();
        alert.show();
    }

    //AFTER THE LOAN CALULATOR CALCULATES THE INSTALLMENTS, THIS FUNCTION IS USED TO PRINT THE RESULTS
    private void displayInstallments() {
        if (interestrate ==0.0) //AVOIDING NAN ERROR BY PRINTING OUTPUT ONLY WHEN BOTH PRICIPLE AND INTEREST ARE ENTERED
        {
            System.out.println("zero");

        } else {
            fivetv.setText(String.valueOf(result5));
            tentv.setText(String.valueOf(result10));
            fifteentv.setText(String.valueOf(result15));
            twentytv.setText(String.valueOf(result20));
            twentyfivetv.setText(String.valueOf(result25));
            thirtytv.setText(String.valueOf(result30));

          /*  fivetv.setText(String.valueOf(Math.round(result5)));
            tentv.setText(String.valueOf(Math.round(result10)));
            fifteentv.setText(String.valueOf(Math.round(result15)));
            twentytv.setText(String.valueOf(Math.round(result20)));
            twentyfivetv.setText(String.valueOf(Math.round(result25)));
            thirtytv.setText(String.valueOf(Math.round(result30)));*/
        }
    }

    //READS THE INTEREST AND CALLS THE LOAN CALCULATOR
    public void setinterest(double i) {
        interestrate = i;
        loancalc();

    }

    //READS THE PRINCIPLE AND CALLS THE LOAN CALCULATOR
    public void setprinciple(double p) {
        principleamt = p;
        loancalc();

    }
}
